####################################################################
# File: GCNLPTutorial.p
# Author: Google
#
# This program is from the Google NLP tutorial.
# Some of this code was utilized in our Preternship project.
###################################################################

#imports from google cloud client library

from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types


# instantiates a client

client = language.LanguageServiceClient()

#the text to analyze
text = u'Hello, world!'
document = types.Document(
	content=text,
	type=enums.Document.Type.PLAIN_TEXT)

#detects sentiment of the text
sentiment = client.analyze_sentiment(document=document).document_sentiment

print('Text : {}'.format(text))
print('Sentiment: {} {}'.format(sentiment.score, sentiment.magnitude))
