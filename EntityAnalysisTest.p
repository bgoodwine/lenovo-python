#Author: Bridget Goodwine
#Email: bgoodwin@nd.edu
#
# This program is the fifth spiral in the Preternship project. It's partner program is nlpsentiment.p.
#This program reads in the reviews for the Lenovo X1 Thinkpad, preforms entity analysis,
# then analyzes the sentiment of each entity. It reports only entities and their scores if
# they have a high enough salience score, i.e., are important enough to the customer.

# Inputs Google's API Client Libraries
from google.cloud import language_v1
from google.cloud.language_v1 import enums

# Sets up a client
client = language_v1.LanguageServiceClient()

# This reads in the file that contains all of the reviews
f = open("InputFile", "r")
f1 = f.readlines()

# This stores each review its own array:
inpu = []
# This stores each entity name in its own array:
ent = []
# This stores the sentiment score for an entity in the corresponding array:
score = []
# This stores the magnitude of that sentiment score in the corresponding array:
mag = []
# This stores the salience score for each entity in the corresponding array:
sal = []

# Reads in the file and stores each review in an array
for x in f1:
	inpu.append(x)

# Takes all of those reviews and puts them all into one string (whole)
whole = ""
i = 0
arrSize = len(inpu)
while i < arrSize:
	whole = whole + inpu[i]
	i = i + 1

# Sets all of the reviews as the text that will be analyzed by the Google NLP

#PUT THIS BACK IN
#text_content = whole

text_content = "I really like Lenovo, and the computer is great, but the computer is also sometimes slow and I don't like that"

# Cloes file
f.close()




# Sets the type and language for the document
type_ = enums.Document.Type.PLAIN_TEXT
language = "en"

# Sets up the data for the document
document = {"content": text_content, "type": type_, "language": language}
encoding_type = enums.EncodingType.UTF8

# Analyzes the document
response = client.analyze_entity_sentiment(document, encoding_type=encoding_type)

w = 0
# Loops through the entities returned from the API and stores all of them in arrays
for entity in response.entities:
	sentiment = entity.sentiment
	print(w)
	if w == 0:
		ent.append(entity.name)
		sal.append(entity.salience)
		score.append(sentiment.score)
		mag.append(sentiment.magnitude)
		print('first appending')
	i = 0
	while i < len(ent):
		print('i = {}'.format(i))
		if ent[i] == entity.name:
			print('sal[i] = {}'.format(sal[i]))
			sal[i] = (sal[i] + entity.salience) / 2
			print('entity.salience = {}'.format(entity.salience))
			print('average = {}'.format(sal[i]))
			mag[i] = (mag[i] + sentiment.magnitude)/2
			score[i] = (score[i] + sentiment.score)/2
			print('{} equals {}'.format(ent[i], entity.name))
		else:
			print(' in else statememnt...')
			if i == (len(ent) - 1):
				ent.append(entity.name)
				sal.append(entity.salience)
				score.append(sentiment.score)
				mag.append(sentiment.magnitude)
				print('appending now...')
			print('{} does not equal {}'.format(ent[i], entity.name))
		i = i + 1
	w = w + 1
	#ent.append(entity.name)
	#sal.append(entity.salience)
	#score.append(sentiment.score)
	#mag.append(sentiment.magnitude)

# Loops through the salience scores and prints the entities if the salience is high enough for the entity to be relevent
arrSize = len(ent)
i = 0
while i < arrSize:
	if sal[i] > 0.001:
		print("Entity: {}".format(ent[i]))
		print("Salience: {}".format(sal[i]))
		
	i = i + 1


print("Done done done")





