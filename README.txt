This is the README file for the Lenovo Team Preternship project.

The final deliverables are below:
	sentiment.p
	entity.p
	excelsentiment.p
	excelentity.p

They can both be run by running the commandline:
	python nlpentity.p
		or
	python nlpsentiment.p

sentiment.p
	This program reads in the reviews from InputFile, which contains all of the reviews that we compiled. It takes these reviews and preforms
	sentiment analysis on each review individually using Google's NLP API client libraries. It prints each review's sentiment score and 
	corresponding magnitude. The sentiment score is a number between -1 and 1, with -1 being negative emotion, 0 being neutral 
	emotion, and 1 being positive emotion. The magnitude is a number between 1 and infinity, with 0 being weak emotion and a large number
	being strong emotion. It also prints out the average sentiment and magnitude for all of the reviews. This prints out the data
	formatted for the command line. 

entity.p
	This program preforms entity analysis on the reviews from InputFile using Google's NLP API client libraries. It prints out the "relevent"
	entities, or the entities within the reviews that have a salience score that is above a certain threshold. The salience score is a score
	of how relevent the entity is to the text. It preforms that same sentiment analysis on each entity as nlpsentiment.p does on the 
	reviews, but this is specific to that entity and spans all of the reviews. It prints the relevent entities and their salience scores.
	This prints out the data formatted for the command line.

excelsentiment.p
	This program does exactly what sentiment.p does, but formats the data so that it can be easily copied and pasted into Excel
	for data analysis.

excelentity.p
	This program does exactly what entity.p does, but formats the data so that it can be easily copied and pasted into Excel for
	data analysis.
