#####################################################################################################
# File: Spiral01.p
# Author: Notre Dame Lenvo Preternship Team
# Email: bgoodwin@nd.edu
#
# This program was the first spiral in our Preternship project. It both read in text from the file and
# analyzed the text with the Google NLP client library functions.
#####################################################################################################

# Note: This code contains errors. This is not the final demo. It was saved for record puropses.
# Do not try to compile this code. 

# Imports google cloud client library
from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types



#####################################
# Function Name: main
# Pre-conditions: none
# Post-conditions: none
#
# This is the maind driver function for Spiral01.p
#####################################
def main():

	# Instantiates a client
	client = language.LanguageServiceClient()
	

	# Input the reviews to analyze
	f = open("InputFile", "r")
	f1 = f.readlines()
	
	# These arrays store the reviews, the scores, and the magnitudes in corresponding arrays
	reviews = []
	score = []
	mag = []	
	iter = 0

	# Loops through the reviews
	for x in f1:
		# Sets up the data for the document
		document = types.Document(
			content=x,
			type=enums.Document.Type.PLAIN_TEXT)
		# Analyzes the sentiment through Google API
		sentiment = client.analyze_sentiment(document=document).document_sentiment
		iter = iter + 1
		# Adds the review, score, and magnitude to it's array
		reviews.append(x)
		score.append(sentiment.score)
		mag.append(sentiment.magnitude)
	
	# Stores the length of the review array
	arrSize = len(reviews)
	
	# Loops through the reviews and prints out the sentiment and the magnitude for each review
	for i in forrange(arrSize):
		print('Sentiment: {} {}'.format(score[i], mag[i])
	
	# Unused code that was commented out: 
	#for x in score:
	#	print score
	#print('Sentiment magnitudes:')
	#for x in mag:
	#	print mag


if __name__ == "__main__":
	main()


